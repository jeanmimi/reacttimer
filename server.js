/* jshint esversion:6, node:true */
'use strict';
var express = require('express');
var path = require('path');

//create the ap
let app = express();

app.use(express.static(path.join(__dirname,'public')));
app.listen(3000, () => console.log('Expresser server is up'));
